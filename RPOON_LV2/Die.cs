﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPOON_LV2
{
    class Die
    {
        private int numberOfSides;
        /*private Random randomGenerator; */
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides,Random randomGenerator) //task 2
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance(); //task 3 
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1); //task 3
            return rolledNumber;
        }
    }
}
