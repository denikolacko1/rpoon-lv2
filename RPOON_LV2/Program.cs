﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller dices = new DiceRoller(); //task 1
            Random randomGenerator = new Random(); //task 2

            int diceRollerCount = 20;
            for ( int i = 0; i < diceRollerCount; i++)
            {
                dices.InsertDie(new Die(6,randomGenerator));

            }
            dices.RollAllDice();
            IList<int> resultOfThrow = dices.GetRollingResults();
            foreach(int results in resultOfThrow)
            {
                System.Diagnostics.Debug.WriteLine(results);
            }
        }
    }
}
